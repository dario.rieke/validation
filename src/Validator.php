<?php 
namespace DarioRieke\Validation;

use DarioRieke\Validation\ValidatorInterface;
use DarioRieke\Validation\SchemaInterface;
use DarioRieke\Validation\ValidationContextInterface;
use DarioRieke\Validation\ValidatableInterface;
use DarioRieke\Validation\Violation\ViolationListInterface;
use DarioRieke\Validation\Exception\InvalidArgumentException;



/**
 * Validator
 */
class Validator implements ValidatorInterface {

	/**
	 * schema to validate against
	 * @var SchemaInterface
	 */
	protected $schema;

	/**
	 * the context to hold information about the validation
	 * @var ValidationContextInterface
	 */
	protected $context;

	/**
	 * @var ValidatorFactoryInterface
	 */
	protected $validatorFactory;

	/**
	 * create a new Validator
	 * @param ValidationContextInterface $context
	 * @param ValidatorFactoryInterface  $validatorFactory
	 */
	public function __construct(ValidationContextInterface $context, ValidatorFactoryInterface $validatorFactory) {
		$this->context = $context;
		$this->validatorFactory = $validatorFactory;
		$context->setValidator($this);
	} 

	public function setSchema(SchemaInterface $schema) {
		$this->schema = $schema;		
	}

	public function getSchema(): ?SchemaInterface {
		return $this->schema;		
	}

	public function getContext(): ValidationContextInterface {
		return $this->context;
	}

	public function setContext(ValidationContextInterface $context) {
		$this->context = $context;
	}


	public function validate($data): ViolationListInterface {
		
		//stack consists of the schema, the corresponding data structure to validate, the path of the current data structure and a boolean paraqm indicating if a node was supplied (checking if the node is null is not enough as the node can actually be null)
		
		//check if a schema is provided
		if($this->schema === null) {
			if($data instanceof ValidatableInterface) {
				$this->setSchema($data->getValidationSchema());
			}
			else {
				$interface = ValidatableInterface::class;
				throw new InvalidArgumentException("You have to provide a schema to validate against, or pass in an Implementation of $interface.");
				
			}
		}

		//set root path if it is not already set
		if(!$this->context->getRootPath()) {
			$this->context->setRootPath($this->getIdentifierForData($data));			
		}

		//first iteration is always valid
		$valid = true; 

		$stack = [[$this->getSchema(), &$data, $this->context->getRootPath(), $valid]];

		$this->context->setCurrentValueExists($valid);

		//loop over the whole stack
		while ($row = array_pop($stack)) {
			$schema = $row[0];
			$node = & $row[1];
			$path = $row[2];
			$valid = $row[3];

			//prepare context for constraints
			$this->context->setCurrentValue($node);
			$this->context->setCurrentValueExists($valid);
			$this->context->setPath($path);

			//validate the current property
			$this->validateSchema($schema, $node);

			// //debug
			// echo "path: ";
			// var_dump($path);
			// echo "\n";
			// echo "node exists: ";
			// var_dump($this->context->currentValueExists());
			// echo "\n";
			// echo "node: ";
			// var_dump($node);
			// echo "\n";
			// echo "schema: ";
			// var_dump($schema);
			// echo "***************************************************************\n";

			//add children to stack
			if($schema->hasChildren()) {

				foreach ($children = $schema->getChildren() as $propertyName => $schema) {

					$resolved = false;

					if(is_object($node)) {
						if(isset($node->{$propertyName}) || property_exists($node, $propertyName)) {
							try {
								$value = $this->getValueFromClassProperty($node, $propertyName);
								// this value is not passed by reference
								$stack[] = [$schema, $value, "$path".'.'.$propertyName, true];
								$resolved = true;
							}
							catch (\ReflectionException $e) {
								// could not resolve the value via reflection
								$resolved = false;
							}
						}
					}
					elseif( (is_array($node) || $node instanceof \ArrayAccess) && array_key_exists($propertyName, $node)) {
						$stack[] = [$schema, & $node[$propertyName], "$path".'['.$propertyName.']', true];
						$resolved = true;
					}

					//node does not exist
					if(!$resolved) {
						$stack[] = [$schema, null, "$path".$propertyName, false];						
					}
				}
			}
		}

		return $this->context->getViolations();
	}

	public function validateSchema(SchemaInterface $schema, $data) {
		//validate all constraints
		foreach ($schema->getConstraints() as $constraint) {
			$constraint->initialize($this->context);
			$constraint->validate($data);
		}
	}

	public function new(): self {
		return $this->validatorFactory->getValidator();
	}

	/**
	 * get a value for a given objects property 
	 * @param  object $object        	the object to get the value for
	 * @param  string $propertyName  	name of the property to access
	 * @throws \ReflectionException     in case the propertys value cant be resolved
	 * @return mixed 					the value of the property
	 */
	protected function getValueFromClassProperty(object $object, string $schemaName) {
		$ref = new \ReflectionObject($object);
		$schema = $ref->getProperty($schemaName);
		$schema->setAccessible(true);
		
		return $schema->getValue($object);
	}


	/**
	 * get the identifier for the given data structure
	 * @param  mixed  $data 
	 * @return string       the identifier 
	 *
	 */
	protected function getIdentifierForData($data): string {
		if(is_array($data)) {
			return 'array';
		}
		elseif(is_object($data)) {
			$className = get_class($data);
			return 'object('.$className.')';
		}
		else {
			return '';
		}
	}
}


