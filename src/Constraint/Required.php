<?php 
namespace DarioRieke\Validation\Constraint;

use DarioRieke\Validation\Constraint\AbstractConstraint;
use DarioRieke\Validation\Violation\Violation;

/**
 * Required Constraint
 *
 * validate if a Property exists
 */ 
class Required extends AbstractConstraint {
	
	public function validate($value): bool {
		if($this->valueExists()) {
			return true;
		}
		else {
			$this->context->addViolation(
				Violation::new()
					->atPath($this->context->getPath())
					->setMessageTemplate('Schema is required but does not exist.')
			);
			return false;
		}
	}
	
}