<?php 
namespace DarioRieke\Validation\Constraint;

use DarioRieke\Validation\ValidationContextInterface;


/**
 * ConstraintInterface 
 *
 * the Constraint is used to add a validation Rule to a SchemaInterface
 */
interface ConstraintInterface {
	
	/**
	 * pass the validation context which holds extra information
	 * @param  ValidationContextInterface $context
	 */
	public function initialize(ValidationContextInterface $context);

	/**
	 * gets the current Context in which the constraint is called
	 * @return ValidationContextInterface|null
	 */
	public function getContext(): ?ValidationContextInterface;

	/**
	 * check if a value matches the constraint
	 * if the validation fails, it MUST add a violation to the ValidatonContextInterface with the addViolation method
	 * @see DarioRieke\Validation\ValidationContext
	 * @return boolean
	 */
	public function validate($value): bool;
	
}