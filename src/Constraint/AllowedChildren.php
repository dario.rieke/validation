<?php 
namespace DarioRieke\Validation\Constraint;

use DarioRieke\Validation\Constraint\AbstractConstraint;
use DarioRieke\Validation\Violation\Violation;

/**
 * AllowedChildren Constraint
 *
 * validate which Children are allowed for this Property
 */ 
class AllowedChildren extends AbstractConstraint {
	/**
	 * array of allowed children as value
	 * @var array
	 */
	private $allowed;

	/**
	 * pass in options to specifiy which children are allowed
	 * @param array $allowed array holding the names of valid children as key
	 * $allowed = [ 'name', 'phone', 'test'];
	 */
	public function __construct(array $allowed) {
		$this->allowed = $allowed;
	}

	public function validate($value): bool {
		//only check the value if it actually exists
		if($this->valueExists()) {
			if(is_iterable($value) || is_array($value) || is_object($value)) {
				//iterate over children and check if they are in the allowed range
				$diff = array_diff_key((array) $value, array_flip($this->allowed));
				if($diff) {
					foreach ($diff as $key => $value) {
							$this->context->addViolation(
								Violation::new()
									->atPath($this->context->getPath().$key)
									->setMessageTemplate('Porperty {{key}} is not allowed.')
									->setParameter('{{key}}', $key)
							);
					}

					return false;
				}
				return true;
			}
			else {
				return false;
			}
		}
		else {
			return false;
		}
	}
	
}