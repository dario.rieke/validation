<?php 
namespace DarioRieke\Validation\Constraint;

use DarioRieke\Validation\Constraint\AbstractConstraint;
use DarioRieke\Validation\Violation\Violation;
use DarioRieke\Validation\ValidatableInterface;
use DarioRieke\Validation\Exception\InvalidArgumentException;

/**
 * Valid Constraint
 * used to validate objects implementing Validation\ValidatableInterface
 */ 
class Valid extends AbstractConstraint {
	/**
	 * @throws  InvalidArgumentException
	 */
	public function validate($value): bool {
		if($this->valueExists() && $value !== null) {
			if($value instanceof ValidatableInterface) {

				$parentValidator = $this->context->getValidator();
				$validator = $parentValidator->new();
				$context = $validator->getContext();
				$parentContext = $this->context;

				$context->setRootPath($parentContext->getPath());
				$violations = $validator->validate($value);

				$this->context->mergeViolations($violations);

				return (count($violations) === 0);
			}
			else {
				//throws an exception for easier debugging
				$interface = ValidatableInterface::class;
				throw new InvalidArgumentException("Provided value does not implement $interface and can not be validated.");
				return false;
			}
		}
		else {
			return false;
		}
	}
}