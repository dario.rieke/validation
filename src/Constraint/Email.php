<?php 
namespace DarioRieke\Validation\Constraint;

use DarioRieke\Validation\Constraint\AbstractConstraint;
use DarioRieke\Validation\Violation\Violation;

/**
 * Email Constraint
 *
 * validate a email address
 */ 
class Email extends AbstractConstraint {
	
	public function validate($value): bool {
		if($this->valueExists()) {

			$valid = filter_var($value, \FILTER_VALIDATE_EMAIL);
			if(!$valid) {
				$this->context->addViolation(
					Violation::new()
						->atPath($this->context->getPath())
						->setMessageTemplate('Value must be a valid email adress.')
				);
			}
			return $valid;
		} 
		else {
			return false;
		}
	}
	
}