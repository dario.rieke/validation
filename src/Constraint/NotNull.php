<?php 
namespace DarioRieke\Validation\Constraint;

use DarioRieke\Validation\Constraint\AbstractConstraint;
use DarioRieke\Validation\Violation\Violation;

/**
 * NotNull Constraint
 *
 * validate if a Property is not null
 */ 
class NotNull extends AbstractConstraint {
	
	public function validate($value): bool {
		//only check the value if it actually exists
		if($this->valueExists()) {
			$valid = $value !== null;
			
			if(!$valid) {
				$this->context->addViolation(
					Violation::new()
						->atPath($this->context->getPath())
						->setMessageTemplate('Value must not be null.')
				);
			}
			return $valid;
		}
		else {
			return false;
		}
	}
	
}