<?php 
namespace DarioRieke\Validation\Constraint;

use DarioRieke\Validation\Constraint\AbstractConstraint;
use DarioRieke\Validation\Violation\Violation;
use DarioRieke\Validation\Exception\InvalidArgumentException;

/**
 * Type Constraint
 * validates the data type
 *
 * validate if a Schema exists
 */ 
class Type extends AbstractConstraint {

	/**
	 * name of the type to validate
	 * @var string
	 */
	private $type;

	/**
	 * possible types to validate
	 */
	private $possibleTypes = [
		'string',
		'array',
		'integer',
		'double',
		'object',
		'boolean',
		'resource',
		'NULL'
	];

	/**
	 * @param string $type the expected type, possible values are:
	 *                     - string
	 *                     - array
	 *                     - integer
	 *                     - double (float)
	 *                     - object
	 *                     - boolean
	 *                     - resource
	 *                     - NULL
	 */
	public function __construct(string $type) {
		if(!in_array($type, $this->possibleTypes)) {
			$types = implode(', ', $this->possibleTypes);
			throw new InvalidArgumentException("Passed Type must be one of: {$types}");	
		} 
		
		$this->type = $type;
	}

	/**
	 * getter for type
	 * @return string
	 */
	public function getType(): ?string {
		return $this->type;
	}

	/**
	 * get the type of the processed value
	 * $this->initialize() must be called before to ensure context is set
	 * @return string 	name of the data type
	 */
	private function getTypeOfValue($value): string {
		return gettype($value);

	}
	
	public function validate($value): bool {
		if($this->valueExists()) {
			$valueType = $this->getTypeOfValue($value);
			$valid = ($valueType === $this->type);

			if(!$valid) {
				$this->context->addViolation(
					Violation::new()
						->atPath($this->context->getPath())
						->setMessageTemplate('Value must be of type {{$expectedType}} but is {{$actualType}}.')
						->setParameters([
							'{{$expectedType}}' => $this->type,
							'{{$actualType}}'   => $valueType

						])
				);

			}

			return $valid;
		}
		else {
			return false;

		}
	}
	
}