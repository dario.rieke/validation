<?php 
namespace DarioRieke\Validation\Constraint;

use DarioRieke\Validation\Constraint\ConstraintInterface;
use DarioRieke\Validation\ValidationContextInterface;

/**
 * Constraint
 */ 
abstract class AbstractConstraint implements ConstraintInterface {

	/**
	 * @var ValidationContextInterface
	 */
	protected $context;
	
	abstract public function validate($value): bool;

	public function initialize(ValidationContextInterface $context) {
		$this->context = $context;
	}

	public function getContext(): ?ValidationContextInterface {
		return $this->context;
	}

	/**
	 * check if the provided value actually exists
	 * @return boolean
	 */
	protected function valueExists(): bool {
		return $this->context->currentValueExists();
	}
}