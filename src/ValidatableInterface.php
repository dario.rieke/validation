<?php 
namespace DarioRieke\Validation;

use DarioRieke\Validation\SchemaInterface;

/**
 * Validatable
 *
 * objects implementing validatable are validatable through the validator
 */
interface ValidatableInterface {
	/**
	 * return a Schema representing the schema to validate the object against
	 * its up to the implementation wether the schema is immutable or mutable
	 * 
	 * @return SchemaInterface   the Schema representing the schema to validate the class against
	 */
	public function getValidationSchema(): SchemaInterface;
}