<?php 
namespace DarioRieke\Validation;

use DarioRieke\Validation\ValidatorInterface;


/**
 * ValidatorFactoryInterface to construct a validator
 */
interface ValidatorFactoryInterface {
	/**
	 * create a new validator
	 * @return ValidatorInterface
	 */
	public function getValidator(): ValidatorInterface;
}


