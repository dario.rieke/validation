<?php 
namespace DarioRieke\Validation;

use DarioRieke\Validation\ValidatorInterface;
use DarioRieke\Validation\ValidatorFactoryInterface;
use DarioRieke\Validation\Validator;
use DarioRieke\Validation\ValidationContext;
use DarioRieke\Validation\Violation\ViolationList;


/**
 * ValidatorFactory
 */
class ValidatorFactory implements ValidatorFactoryInterface {
	public function getValidator(): ValidatorInterface {
		return new Validator(
			new ValidationContext(new ViolationList()),
			$this
		);
	}
}


