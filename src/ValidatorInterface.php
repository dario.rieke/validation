<?php 
namespace DarioRieke\Validation;

use DarioRieke\Validation\SchemaInterface;
use DarioRieke\Validation\Violation\ViolationListInterface;
use DarioRieke\Validation\ValidationContextInterface;


/**
 * ValidatorInterface
 *
 * validates data structures against given schemata
 */
interface ValidatorInterface {

	/**
	 * sets the schema to validate against
	 * @param SchemaInterface $schema
	 */
	public function setSchema(SchemaInterface $schema);

	/**
	 * get the schema to validate against
	 * @return SchemaInterface|null
	 */
	public function getSchema(): ?SchemaInterface;

	/**
	 * validate a data structure against a given schema
	 * @param  mixed 					$data 	the data structure to validate
	 * @return ValidationListInterface  		a list of violations, if the list is empty, validation succeeded
	 */
	public function validate($data): ViolationListInterface;

	/**
	 * validate data against a given Schema with constraints
	 * @param  SchemaInterface $schema the schema with all constraints to validate 
	 * @param  mixed     		 $data     data to validate 
	 */
	public function validateSchema(SchemaInterface $schema, $data);

	/**
	 * returns a new Validator Instance 
	 * @return ValidatorFactoryInterface
	 */
	public function new(): self;

	/**
	 * get the current context
	 * @return ValidationContextInterface
	 */
	public function getContext(): ValidationContextInterface;

	/**
	 * set the new context
	 * @param ValidationContextInterface
	 */
	public function setContext(ValidationContextInterface $context);
}


