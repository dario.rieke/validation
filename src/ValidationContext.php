<?php 
namespace DarioRieke\Validation;

use DarioRieke\Validation\ValidationContextInterface;
use DarioRieke\Validation\Violation\ViolationListInterface;
use DarioRieke\Validation\Violation\ViolationInterface;

/**
 * ValidationContext 
 */
class ValidationContext implements ValidationContextInterface {

	/**
	 * violations
	 * @var ViolationListInterface
	 */
	private $violations;

	/**
	 * the current value if it exists
	 * @var null|mixed
	 */
	private $currentValue = null;

	/**
	 * true if a value exists for current schema or false if there is no data
	 * @var boolean
	 */
	private $currentValueExists = false;

	/**
	 * the root path in which the context is used 
	 * @var string
	 */
	private $rootPath = '';

	/**
	 * current path
	 * @var string
	 */
	private $path = '';

	/**
	 * @var ValidatorInterface
	 */
	private $validator;

	/**
	 * create a new ValidationContext 
	 * @param ViolationListInterface $violationList list which holds all Violations
	 */
	public function __construct(ViolationListInterface $violationList) {
		$this->violations = $violationList;
	}

	public function addViolation(ViolationInterface $violation) {
		$this->violations->add($violation);
	}

	public function currentValueExists(): bool {
		return $this->currentValueExists;
	}

	public function setCurrentValue($value) {
		$this->currentValue = $value;
	}

	public function setCurrentValueExists(bool $bool) {
		$this->currentValueExists = $bool;
	}

	public function getCurrentValue() {
		return $this->currentValue;
	}

	public function getPath(): string {
		return $this->path;
	}

	public function setPath(string $path) {
		$this->path = $path;
	}

	public function setRootPath(string $path) {
		$this->rootPath = $path;
	}

	public function getRootPath(): string {
		return $this->rootPath;
	}

	public function getViolations(): ViolationListInterface {
		return $this->violations;
	}

	public function setValidator(ValidatorInterface $validator): void {
		$this->validator = $validator;
	}

	public function getValidator(): ?ValidatorInterface {
		return $this->validator;
	}

	public function mergeViolations(ViolationListInterface $violations) {
		$this->violations->merge($violations);
	}

}


