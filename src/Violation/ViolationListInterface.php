<?php 
namespace DarioRieke\Validation\Violation;

use DarioRieke\Validation\Violation\ViolationInterface;

/**
 * ViolationListInterface
 *
 * list of violations
 */
interface ViolationListInterface extends \Iterator, \Countable {

	/**
	 * add a Violation to the list
	 * @param ViolationInterface $violation 
	 */
	public function add(ViolationInterface $violation);

	public function current(): ViolationInterface;

	/**
	 * merge 2 instances by adding all the violations from the passed list into this instance
	 * @param  ViolationListInterface $violationList
	 */
	public function merge(ViolationListInterface $violationList): void;
}