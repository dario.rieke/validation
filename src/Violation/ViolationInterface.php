<?php 
namespace DarioRieke\Validation\Violation;

/**
 * ViolationInterface
 *
 * an object representing a Violation of a Constraint 
 */
interface ViolationInterface {

	/**
	 * create a new Violation
	 * fluid wrapper for constructor
	 * @return self
	 */
	public function new(): self;

	/**
	 * get the path where the violation occured
	 * @return string
	 */
	public function getPath(): string;

	/**
	 * return the final error message 
	 * @return string
	 */
	public function getMessage(): string;

	/**
	 * get the message template with placeholders for values
	 * @return string
	 */
	public function getMessageTemplate(): string;

	/**
	 * set the template used as a violation message
	 * @param string $template 
	 * @return self
	 */
	public function setMessageTemplate(string $template): self;

	/**
	 * return an associative array of parameters to insert 
	 * @return array 	key is name of the placeholder, value is the value for it
	 */
	public function getParameters(): array;

	/**
	 * set multiple parameters and overwrite existing ones
	 * @param array $params 
	 * @return self
	 */
	public function setParameters(array $params): self;

	/**
	 * set a parameter for the message
	 * @param string $key   placeholder for value
	 * @param mixed  $value value to replace placeholder with
	 * @return self
	 */
	public function setParameter(string $key, $value): self;

	/**
	 * set the path for the current violation
	 * @param  string   $path   path to set
	 * @return self
	 */
	public function atPath(string $path): self;
}


