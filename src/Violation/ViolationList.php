<?php 
namespace DarioRieke\Validation\Violation;

use DarioRieke\Validation\Violation\ViolationInterface;
use DarioRieke\Validation\Violation\ViolationListInterface;

/**
 * iterable ViolationList holding all occured Violations
 */
class ViolationList implements ViolationListInterface {

	/**
	 * array of validations
	 * @var array
	 */
	private $violations = [];

	/**
	 * pointer to current violation
	 * @var int
	 */
	private $current;
	/**
	 * create a new ViolationList
	 */
	public function __construct() {
		$this->current = 0;
	}

	public function add(ViolationInterface $violation) {
		$this->violations[] = $violation;
	}

	public function current(): ViolationInterface {
		return $this->violations[$this->key()];
	}

	public function key() {
		return $this->current;
	}

	public function next() {
		$this->current++;
	}

	public function rewind() {
		$this->current = 0;
	}

	public function valid(): bool {
		return array_key_exists($this->key(), $this->violations);
	}

	public function count(): int {
		return count($this->violations);
	}

	public function merge(ViolationListInterface $violationList): void {
		foreach ($violationList as $violation) {
			$this->add($violation);
		}
	}

}


