<?php 
namespace DarioRieke\Validation\Violation;

use DarioRieke\Validation\Violation\ViolationInterface;

/**
 * Violation
 */
class Violation implements ViolationInterface {

	/**
	 * path were violation happened
	 * @var string
	 */
	private $path;

	/**
	 * message template with placeholders
	 * @var string
	 */
	private $messageTemplate;

	/**
	 * parameters for message
	 * @see Interface getMessageParameters()
	 * @var array
	 */
	private $messageParameters;

	/**
	 * create a new Violation
	 */
	private function __construct() {
		$this->messageParameters = [];
	}

	public function new(): self {
		return new self();
	}

	public function atPath(string $path): self {
		$this->path = $path;
		return $this;
	}

	public function setParameter(string $key, $value): self {
		$this->messageParameters[$key] = $value;
		return $this;
	}

	public function setParameters(array $params): self {
		foreach ($params as $key => $value) {
			$this->setParameter($key, $value);
		}
		return $this;
	}

	public function getPath(): string {
		return $this->path;
	}

	public function getMessage(): string {
		if(!empty($this->messageParameters)) {
			return str_replace(array_keys($this->messageParameters), $this->messageParameters, $this->messageTemplate);
		}
		return $this->messageTemplate;
	}

	public function getMessageTemplate(): string {
		return $this->messageTemplate;
	}

	public function setMessageTemplate(string $template): self {
		$this->messageTemplate = $template;
		return $this;
	}

	public function getParameters(): array {
		return $this->messageParameters;
	}
}


