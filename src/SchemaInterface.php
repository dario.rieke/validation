<?php 
namespace DarioRieke\Validation;

use DarioRieke\Validation\Constraint\ConstraintInterface;

/**
 * SchemaInterface 
 * 
 * Interface represents a Schema/Node used in the schema for the Validator
 * 
 */
interface SchemaInterface {
	/**
	 * add a Constraint to the Schema
	 */
	public function addConstraint(ConstraintInterface $constraint): void;

	/**
	 * removes a constraint from the Schema
	 * should not throw an exception if constraint to rmeove was not found
	 * @param  string 	fully qualified classname of the constraint to remove (should remove all constraints of that type)
	 * @return void
	 */
	public function removeConstraint(string $constraint): void;

	/**
	 * returns the set Constraints 
	 * @return Constraint\ConstraintInterface[]
	 */
	public function getConstraints(): array;

	/**
	 * return the children properties as an array where the key is the name of the property and value is the SchemaInterface
	 * @return SchemaInterface[]
	 */
	public function getChildren(): array;

	/**
	 * @return boolean wether the Schema has children or not
	 */
	public function hasChildren(): bool;
}


