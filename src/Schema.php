<?php 
namespace DarioRieke\Validation;

use DarioRieke\Validation\SchemaInterface; 
use DarioRieke\Validation\Constraint\ConstraintInterface; 

/**
 * Schema
 * 
 */
class Schema implements SchemaInterface {



	/**
	 * 
	 * @var ConstraintInterface[]
	 */
	private $constraints = [];

	/**
	 * 
	 * @var SchemaInterface[]
	 */
	private $children = [];

	/**
	 * create a new Schema
	 * 
	 * @param ConstraintInterface[]		$constraints 	one or multiple constraints to validate against
	 */
	public function __construct(ConstraintInterface ...$constraints) {
		$this->constraints = $constraints;
	}


	/**
	 * add a Constraint to the Schema
	 */
	public function addConstraint(ConstraintInterface $constraint): void {
		$this->constraints[] = $constraint;
	}

	public function removeConstraint(string $classname): void {
		foreach ($this->constraints as $i => $constraint) {
			if(get_class($constraint) === $classname) unset($this->constraints[$i]);
		}
	}

	/**
	 * returns the set Constraints 
	 * @return Constraint\ConstraintInterface[]
	 */
	public function getConstraints(): array {
		return $this->constraints;
	}

	/**
	 * add a children to the current schema
	 * @param string            $name      name of the schema | used as the identifier
	 * @param SchemaInterface $schema 
	 */
	public function addChild(string $name, SchemaInterface $schema): void {
		$this->children[$name] = $schema;
	}

	/**
	 * return the child schema
	 * @return SchemaInterface[]
	 */
	public function getChildren(): array {
		return $this->children;
	}

	/**
	 * @return boolean wether the Schema has children or not
	 */
	public function hasChildren(): bool {
		return !empty($this->children);
	}
}


