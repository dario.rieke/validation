<?php 
namespace DarioRieke\Validation;

use DarioRieke\Validation\Violation\ViolationListInterface;
use DarioRieke\Validation\Violation\ViolationInterface;
use DarioRieke\Validation\ValidatorInterface;

/**
 * ValidationContextInterface 
 * 
 * holds information about the Validation and all Violations which occured
 * values and return values depend on the current depth and place of validation, so they change over its lifetime 
 */
interface ValidationContextInterface {

	/**
	 * set the current validator
	 * @param ValidatorInterface $validator
	 */
	public function setValidator(ValidatorInterface $validator): void;

	/**
	 * get the current validator
	 * @return ValidatorInterface
	 */
	public function getValidator(): ?ValidatorInterface;

	/**
	 * add a violation to the list
	 * @param ViolationInterface $violation violation to add
	 */
	public function addViolation(ViolationInterface $violation);

	/**
	 * check if the current value for this property/element exists
	 *
	 * if returns false the value for this property to validate doesnt exist
	 * @return bool
	 */
	public function currentValueExists(): bool;

	/**
	 * set if there is a value for current schema or not
	 * @param bool $bool
	 */
	public function setCurrentValueExists(bool $bool);

	/**
	 * get the current value which is validated
	 * should only be called after it is ensured that the value exists via {@link currentValueExists()} because the value can actually be NULL
	 * @return mixed
	 */
	public function getCurrentValue();

	/**
	 * set the current value which is validated
	 * @param mixed $value 
	 */
	public function setCurrentValue($value);

	/**
	 * returns the current path/pointer to the current property/element
	 * @return string 
	 */
	public function getPath(): string;

	/**
	 * set the current path
	 * @param string $path 
	 */
	public function setPath(string $path);

	/**
	 * set the root path
	 * @param string $path 
	 */
	public function setRootPath(string $path);

	/**
	 * get the root path
	 * @return string
	 */
	public function getRootPath(): string;

	/**
	 * get all validations which occured until now
	 * @return ViolationListInterface
	 */
	public function getViolations(): ViolationListInterface;

	/**
	 * return a new ValidationContext
	 * @return self
	 */
	// public function new(): self;

	/**
	 * merge another violation list into the current one
	 * @param  ViolationListInterface $violations 
	 */
	public function mergeViolations(ViolationListInterface $violations);
}


