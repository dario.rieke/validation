<?php 
declare(strict_types=1);
namespace DarioRieke\Validation\Tests\Violation;

use PHPUnit\Framework\TestCase;
use DarioRieke\Validation\Violation\ViolationInterface;
use DarioRieke\Validation\Violation\ViolationList;
use DarioRieke\Validation\Violation\ViolationListInterface;


class ViolationListTest extends TestCase {

	public function setUp(): void {
		$this->violationList = $this->getViolationList();
	}

	protected function getViolationList() {
		return new ViolationList();
	}

	protected function getViolation() {
		return $this->createMock(ViolationInterface::class);
	}

	public function testImplementsViolationListInterface() {
		$this->assertInstanceOf(ViolationListInterface::class, $this->violationList);
	}

	public function testCanAddAndCountViolations() {
		$this->violationList->add($this->getViolation());
		$this->violationList->add($this->getViolation());

		$this->assertCount(2, $this->violationList);

		return clone $this->violationList;
	}

	/**
	 * @depends testCanAddAndCountViolations
	 */
	public function testIsIterable($violationList) {
		$counter = 0;
		foreach ($violationList as $i => $violation) {
			$counter++;
			$this->assertInstanceOf(ViolationInterface::class, $violation);
		}

		$this->assertSame(2, $counter);

		foreach ($violationList as $i => $violation) {
			$counter++;
			$this->assertInstanceOf(ViolationInterface::class, $violation);
		}

		$this->assertSame(4, $counter);
	}

	/**
	 * @depends testCanAddAndCountViolations
	 */
	public function testCanMergeViolationLists($violationList) {
		$this->violationList->add($this->getViolation());
		$this->violationList->merge($violationList);

		$this->assertCount(3, $this->violationList);
	}
}