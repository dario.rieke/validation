<?php 
declare(strict_types=1);
namespace DarioRieke\Validation\Tests\Violation;

use PHPUnit\Framework\TestCase;
use DarioRieke\Validation\Violation\Violation;
use DarioRieke\Validation\Violation\ViolationInterface;


class ViolationTest extends TestCase {

	public function setUp(): void {
		$this->violation = $this->getViolation();
	}

	protected function getViolation() {
		return Violation::new();
	}

	public function testImplementsViolationInterface() {
		$this->assertInstanceOf(ViolationInterface::class, $this->violation);
	}

	public function testIsFluidInterface() {
		$this->assertInstanceOf(ViolationInterface::class, $this->violation->atPath(''));
		$this->assertInstanceOf(ViolationInterface::class, $this->violation->setParameter('', ''));
		$this->assertInstanceOf(ViolationInterface::class, $this->violation->setParameters([]));
		$this->assertInstanceOf(ViolationInterface::class, Violation::new());
	}

	public function testCanCreateNewViolation() {
		$this->assertInstanceOf(ViolationInterface::class, Violation::new());
	}

	public function testCanGetAndSetPath() {
		$this->assertInstanceOf(ViolationInterface::class, $this->violation->atPath('test'));
		$this->assertSame('test', $this->violation->getPath());
	}

	public function testCanGetAndSetMessageTemplate() {
		$this->assertInstanceOf(ViolationInterface::class, $this->violation->setMessageTemplate('this is a test template: {{value}}.'));
		$this->assertSame('this is a test template: {{value}}.', $this->violation->getMessageTemplate());

		return $this->violation;
	}

	public function testCanGetAndSetParameters() {
		$this->assertInstanceOf(ViolationInterface::class, $this->violation->setParameter('{{value}}', 'VALUE'));
		$this->assertInstanceOf(ViolationInterface::class, $this->violation->setParameters(['{{value1}}' => 'VALUE1', '{{value2}}' => 'VALUE2']));
		$params = $this->violation->getParameters();
		$expected = [
			'{{value}}' => 'VALUE',
			'{{value1}}' => 'VALUE1',
			'{{value2}}' => 'VALUE2'
		];
		$this->assertCount(3, $params);
		$this->assertEqualsCanonicalizing($expected, $params);
	}

	// public function 

	public function testCanAddAndReturnPath() {
		$this->assertInstanceOf(ViolationInterface::class, $this->violation->atPath('test'));
		$this->assertSame($this->violation->getPath(), 'test');
	}
}