<?php 
declare(strict_types=1);
namespace DarioRieke\Validation\Tests;

use PHPUnit\Framework\TestCase;
use DarioRieke\Validation\Validator;
use DarioRieke\Validation\ValidatorInterface;
use DarioRieke\Validation\ValidationContextInterface;
use DarioRieke\Validation\Constraint\ConstraintInterface;
use DarioRieke\Validation\Violation\ViolationListInterface;
use DarioRieke\Validation\SchemaInterface;
use DarioRieke\Validation\ValidatableInterface;
use DarioRieke\Validation\Exception\InvalidArgumentException;
use DarioRieke\Validation\ValidatorFactoryInterface;



class ValidatorTest extends TestCase { 

	public function setUp(): void {
		$this->validator = $this->getValidatorWithMock();
	}

	protected function getValidatorWithMock() {
		return new Validator($this->getValidationContextMock(), $this->getValidatorFactoryMock());
	}

	protected function getValidatorFactoryMock() {
		return $this->createMock(ValidatorFactoryInterface::class);
	}

	protected function getConstraintExpectingValidateCall() {
		$mock = $this->createMock(ConstraintInterface::class);
		$mock->expects($this->atLeastOnce())->method('validate');

		return $mock;
	}

	protected function getValidationContextMock() {
		return $this->createMock(ValidationContextInterface::class);
	}

	protected function getSchemaMock() {
		return $this->createMock(SchemaInterface::class);
	}

	protected function getValidatableInterfaceMock() {
		$mock = $this->createMock(ValidatableInterface::class);
		$mock->expects($this->any())->method('getValidationSchema')->will($this->returnValue($this->getSchemaMock()));
		return $mock;
	}

	public function testImplementsValidatorInterface() {
		$this->assertInstanceOf(ValidatorInterface::class, $this->validator);
	}

	public function testCanSetAndGetSchema() {
		$this->assertSame(null, $this->validator->getSchema());

		$this->validator->setSchema($this->getSchemaMock());

		$this->assertInstanceOf(SchemaInterface::class, $this->validator->getSchema());

		return $this->validator;
	}

	/**
	 * @depends testCanSetAndGetSchema
	 */
	public function testReturnsViolationListOnValidation($validator) {
		$this->assertInstanceOf(ViolationListInterface::class, $validator->validate('string'));
	}

	public function testThrowsExceptionIfNoSchemaSupplied() {
		$this->expectException(InvalidArgumentException::class);
		$this->validator->validate(new \stdClass());
	}

	public function testGetSchemaFromValidatableInterface() {
		$this->validator->validate($this->getValidatableInterfaceMock());
		$this->assertInstanceOf(SchemaInterface::class, $this->validator->getSchema());
	}

	public function testCallsConstraintsValidateMethodOnSchemaValidation() {
		$constraints = [ $this->getConstraintExpectingValidateCall(), $this->getConstraintExpectingValidateCall(), $this->getConstraintExpectingValidateCall() ];

		$schemaMock = $this->getSchemaMock();
		$schemaMock->expects($this->any())->method('getConstraints')->will($this->returnValue($constraints)); 
		$this->validator->validateSchema($schemaMock, 'data');
	}
}