<?php 

namespace DarioRieke\Validation\Tests\Fixtures;

use DarioRieke\Validation\Schema;
use DarioRieke\Validation\SchemaInterface;
use DarioRieke\Validation\ValidatableInterface;
use DarioRieke\Validation\Constraint\NotNull;

/**
 * class used to test validation on objects
 */
class Validatable implements ValidatableInterface {

	private $privateSchema;

	protected $protectedSchema;

	public $publicSchema;

	/**
	 * @var SchemaInterface
	 */
	private $validationSchema;


	public function __construct() {
		$this->validationSchema = new Schema();

		$this->validationSchema->addChild('privateSchema', new Schema(
			new NotNull()
		));
		$this->validationSchema->addChild('protectedSchema', new Schema(
			new NotNull()
		));
		$this->validationSchema->addChild('publicSchema', new Schema(
			new NotNull()
		));
	}

	/**
	 * the default schema requires all properties to be set
	 * change the 
	 */
	public function getValidationSchema(): SchemaInterface {
		return $this->validationSchema;
	}

	public function setProtectedSchema($value) {
		$this->protectedSchema = $value;
	}

	public function setPrivateSchema($value) {
		$this->privateSchema = $value;
	}
}