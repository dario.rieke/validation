<?php 
declare(strict_types=1);
namespace DarioRieke\Validation\Tests\Constraint;


use DarioRieke\Validation\Tests\Constraint\AbstractConstraintTest;
use DarioRieke\Validation\Constraint\NotNull;

final class NotNullTest extends AbstractConstraintTest {

	public function setUp(): void {
	    $this->constraint = new NotNull();
	}

	/**
	 * @dataProvider getNotNullTypes
	 */
	public function testCanValidateNotNullValue($validValue) {
		$this->constraint->initialize($this->getValidationContextWithValue());
		$this->assertTrue($this->constraint->validate($validValue));
	}

	public function testCanNotValidateNullValue() {
		$value = null;

		$context = $this->getValidationContextWithValue();
		$this->makeContextExpectViolation($context);
		$this->constraint->initialize($context);

		$this->assertFalse($this->constraint->validate($value));
	}

	public function getNotNullTypes() {
		return [
			'string' => ['string'],
			'0 (int)' =>[0],
			'int' => [1],
			'null (string)' => ['null'],
			'empty array' => [array()]
		];
	}
}