<?php 
declare(strict_types=1);
namespace DarioRieke\Validation\Tests\Constraint;


use DarioRieke\Validation\Tests\Constraint\AbstractConstraintTest;
use DarioRieke\Validation\Constraint\Email;

final class EmailTest extends AbstractConstraintTest {

    static private $invalid = 'invalidemail';
    static private $valid   = 'test@gmail.com';

    public function setUp():void {
        $this->constraint = new Email();
    }

    public function testCanValidateValidEmailAddress() {
        $this->constraint->initialize($this->getValidationContextWithValue());
        $this->assertTrue($this->constraint->validate(self::$valid));
    }

    public function testCanNotValidateInvalidEmailAddress() {
        $this->constraint->initialize($this->getValidationContextWithValue());
        $this->assertFalse($this->constraint->validate(self::$invalid));
    }

    /**
     * @depends testCanNotValidateInvalidEmailAddress
     */
    public function testCanAddViolationToViolationListIfInitializedAndValidationFails() {
        $context = $this->getValidationContextWithValue();
        $this->makeContextExpectViolation($context);
        $this->constraint->initialize($context);
        $this->constraint->validate(self::$invalid);
    }
}