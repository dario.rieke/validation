<?php 
declare(strict_types=1);
namespace DarioRieke\Validation\Tests\Constraint;


use DarioRieke\Validation\Tests\Constraint\AbstractConstraintTest;
use DarioRieke\Validation\Constraint\AllowedChildren;

final class AllowedChildrenTest extends AbstractConstraintTest { 

	public function setUp(): void {
	    $this->constraint = new AllowedChildren(['string']);
	}

	/**
	 * @dataProvider getValidChildren
	 */
	public function testCanValidateAllowedChildren($allowed, $given) {
		$constraint = new AllowedChildren($allowed);
		$context = $this->getValidationContextWithValue();
		$constraint->initialize($context);

		$this->assertTrue($constraint->validate($given));
	}

	/**
	 * @dataProvider getInvalidChildren
	 */
	public function testCanNotValidateNotAllowedChildren($allowed, $given) {
		$constraint = new AllowedChildren($allowed);
		$context = $this->getValidationContextWithValue();
		$this->makeContextExpectViolation($context);
		$constraint->initialize($context);

		$this->assertFalse($constraint->validate($given));
	}

	public function getValidChildren() {

		$object = new \stdClass();
		$object->child1 = true;
		$object->child2 = 'test';
		$object->child3 = false;

		return [
			'different children (array)' => [
				[ 'child1', 'child2', 'child3' ],
				[ 'child1' => 0, 'child2' => 23, 'child3' => 'str' ]
			],
			'different children (\stdClass::class)' => [
				[ 'child1', 'child2', 'child3' ],
				$object
			]
		];
	}

	public function getInvalidChildren() {

		$object = new \stdClass();
		$object->child1 = true;
		$object->child2 = 'test';
		$object->child3 = false;
		$object->child4 = false;
		$object->child5 = false;

		return [
			'same children (array)' => [
				[ 'child1', 'child2', 'child3' ],
				[ 'child1' => 0, 'child2' => 23, 'child' => 'str', 'child4' => 0 ]
			],
			'same children (\stdClass::class)' => [
				[ 'child1', 'child2', 'child3' ],
				$object
			]
		];
	}
}