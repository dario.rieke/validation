<?php 
declare(strict_types=1);
namespace DarioRieke\Validation\Tests\Constraint;

use PHPUnit\Framework\TestCase;

use DarioRieke\Validation\Constraint\AbstractConstraint;
use DarioRieke\Validation\Constraint\ConstraintInterface;
use DarioRieke\Validation\ValidationContextInterface;
use DarioRieke\Validation\Violation\ViolationInterface;

/**
 * extend this class to test a Constraint with some default tests for every constraint
 */
abstract class AbstractConstraintTest extends TestCase {
    /**
     * non abstract implementation of Constraint
     * @var Constraint
     */
    protected $constraint;

    /**
     * set $this->constraint to a class extending \Constraint in a child class
     * do not call $constraint->initialize  
     */
    public function setUp(): void {
        $this->constraint = new class extends AbstractConstraint {
            public function validate(): bool {
                return true;
            }
        };
    }

    public function testImplementsConstraintInterface() {
        $this->assertInstanceOf(ConstraintInterface::class, $this->constraint);
    }

    public function testCanBeInitialized() {
        $this->initialize();

        $this->assertInstanceOf(ValidationContextInterface::class, $this->constraint->getContext());
    }

    public function testcanNotReturnContextIfNotInitialized() {
        $this->assertTrue($this->constraint->getContext() === null); 
    }

    /**
     * helper to simulate a initialize call
     */
    protected function initialize() {
        $context = $this->createMock(ValidationContextInterface::class);
        $this->constraint->initialize($context);
    }
    /**
     * initialize the context with a validation context mockup which expects to get at least one violation added to it (should happen if violation fails)
     */
    protected function initializeForViolationAdding() {
        $context = $this->createMock(ValidationContextInterface::class);
        $this->makeContextExpectViolation($context);

        $this->constraint->initialize($context);
    }

    /**
     * alter the context to expect a violation added via addViolation
     * should be called in tests where $constraint->validate returns false
     */
    protected function makeContextExpectViolation(ValidationContextInterface $context) {
        $context->expects($this->atLeastOnce())->method('addViolation')->with($this->isInstanceOf(ViolationInterface::class));
    }

    /**
     * returns a ValidationContextInterface Mock with currentValueExists true set for testing
     * @return ValidationContextInterface
     */
    protected function getValidationContextWithValue() {
        $context = $this->createMock(ValidationContextInterface::class);
        $context->expects($this->any())->method('currentValueExists')->will($this->returnValue(true));
        return $context;
    }
    /**
     * returns a ValidationContextInterface Mock with currentValueExists false set for testing
     * simulates a missing value
     * @return ValidationContextInterface
     */
    protected function getValidationContextWithoutValue() {
        $context = $this->createMock(ValidationContextInterface::class);
        $context->expects($this->any())->method('currentValueExists')->will($this->returnValue(false));
        return $context;
    }
}