<?php 
declare(strict_types=1);
namespace DarioRieke\Validation\Tests\Constraint;


use DarioRieke\Validation\Tests\Constraint\AbstractConstraintTest;
use DarioRieke\Validation\Constraint\Type;
use DarioRieke\Validation\Exception\InvalidArgumentException;

final class TypeTest extends AbstractConstraintTest {

    public function setUp(): void {
        $this->constraint = new Type('string');
    }
    
    /**
     * @dataProvider getValidTypes
     */
    public function testCanValidateMatchingType($type, $value) {
        $constraint = new Type($type);
        $context = $this->getValidationContextWithValue();
        $constraint->initialize($context);

        $this->assertTrue($constraint->validate($value));
    }

    /**
     * @dataProvider getInvalidTypes
     */
    public function testCanNotValidateWrongType($type, $value) {
        $constraint = new Type($type);

        $context = $this->getValidationContextWithValue();
        $constraint->initialize($context);

        $this->makeContextExpectViolation($context);
        $this->assertFalse($constraint->validate($value));
    }

    public function testThrowsExceptionIfInvalidArgumentPassed() {
        $this->expectException(InvalidArgumentException::class);
        new Type('invalidType');
    }

    /**
     * data type provider
     */
    public static function getValidTypes() {
        return [
            'array' => ['array', array('value')],
            'string' => ['string', 'test'],
            'integer' => ['integer', 3],
            'double' => ['double', 2.34],
            'object' => ['object', (object) array()],
            'boolean' => ['boolean', false],
            'resource' => ['resource', fopen('php://memory', 'r+')],
            'NULL' => ['NULL', null]

        ];
    }

    /**
     * data type provider
     */
    public static function getInvalidTypes() {
        return [
            'array' => ['array', 'abc'],
            'string' => ['string', 4],
            'integer' => ['integer', 2.34],
            'double' => ['double', 2],
            'object' => ['object', array()],
            'boolean' => ['boolean', 1],
            'resource' => ['resource', 'test'],
            'NULL' => ['NULL', 1]

        ];
    }
}