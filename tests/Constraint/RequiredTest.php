<?php 
declare(strict_types=1);
namespace DarioRieke\Validation\Tests\Constraint;


use DarioRieke\Validation\Tests\Constraint\AbstractConstraintTest;
use DarioRieke\Validation\Constraint\Required;

final class RequiredTest extends AbstractConstraintTest {

    public function setUp():void {
        $this->constraint = new Required();
    }

    public function testCanValidatePresenceOfValue() {

        $context = $this->getValidationContextWithValue();
        $this->constraint->initialize($context);

        $this->assertTrue($this->constraint->validate('test'));
    }

    public function testCanNotValidateAbsenceOfValue() {

        $context = $this->getValidationContextWithoutValue();
        $this->constraint->initialize($context);

        $this->makeContextExpectViolation($context);
        $this->assertFalse($this->constraint->validate('test'));
    }
}