<?php 
declare(strict_types=1);
namespace DarioRieke\Validation\Tests\Constraint;


use DarioRieke\Validation\Tests\Constraint\AbstractConstraintTest;
use DarioRieke\Validation\Constraint\Valid;
use DarioRieke\Validation\ValidatorInterface;
use DarioRieke\Validation\Violation\ViolationListInterface;
use DarioRieke\Validation\ValidatableInterface;
use DarioRieke\Validation\Exception\InvalidArgumentException;


final class ValidTest extends AbstractConstraintTest {

    public function setUp():void {
        $this->constraint = new Valid();
    }

    public function testThrowsExceptionIfValuExistsButDoesNotImplementViolationInterface() {
        $this->expectException(InvalidArgumentException::class);

        $context = $this->getValidationContextWithValue();
        $this->constraint->initialize($context);

        $invalid = array(1,2,3);
        $this->constraint->validate($invalid);
    }

    public function testCallsValidatorWithProvidedValue() {
        $value = $this->getValidatableMock();
        $context = $this->getValidationContextWithValue();
        $validator = $this->getValidatorMock();

        $validator->expects($this->atLeastOnce())->method('validate')->with($this->equalTo($value))->willReturn($this->getViolationListMock());
        $validator->expects($this->any())->method('new')->will($this->returnValue($validator));
        $context->expects($this->atLeastOnce())->method('getValidator')->will($this->returnValue($validator));

        $this->constraint->initialize($context);
        $this->constraint->validate($value);

        return $context;
    }

    /**
     * @depends testCallsValidatorWithProvidedValue
     */
    public function testMergesViolationsIntoValidator($context) {
        $context->expects($this->atLeastOnce())->method('mergeViolations')->with($this->isInstanceOf(ViolationListInterface::class));
        $this->constraint->initialize($context);
        $this->constraint->validate($this->getValidatableMock());
    }

    protected function getValidatorMock() {
        return $this->createMock(ValidatorInterface::class);
    }

    protected function getViolationListMock() {
        return $this->createMock(ViolationListInterface::class);
    } 

    protected function getValidatableMock() {
        return $this->createMock(ValidatableInterface::class);  
    }
}