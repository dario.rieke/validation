<?php 
declare(strict_types=1);
namespace DarioRieke\Validation\Tests;

use PHPUnit\Framework\TestCase;

use DarioRieke\Validation\Schema;
use DarioRieke\Validation\SchemaInterface;
use DarioRieke\Validation\Constraint\ConstraintInterface;
use DarioRieke\Validation\Constraint\Email;


class SchemaTest extends TestCase { 
	public function testImplementsSchemaInterface() {
		$this->assertInstanceOf(SchemaInterface::class, $this->getSchema());
	}

	public function testCanAddChild() {
		$schema = $this->getSchema();
		$schema->addChild('name', $this->getSchema());

		$this->assertTrue(true);

		return $schema;
	}

	/**
	 * @depends testCanAddChild
	 */
	public function testHasChildrenIfChildrenAdded($schema) {
		$this->assertTrue($schema->hasChildren());

		return $schema;
	}

	/**
	 * @depends testHasChildrenIfChildrenAdded
	 */
	public function testCanReturnChildrenIfHasChildren($schema) {
		$this->assertContainsOnlyInstancesOf(SchemaInterface::class, $schema->getChildren());
	}

	public function testCanAddConstraint() {
		$schema = $this->getSchema();
		$schema->addConstraint($this->getConstraint());

		$this->assertTrue(true);
		return $schema;
	}

	/**
	 * @depends testCanAddConstraint
	 */
	public function testCanReturnConstraints($schema) {
		$constraints = $schema->getConstraints();

		$this->assertContainsOnlyInstancesOf(ConstraintInterface::class, $constraints);
		$this->assertCount(1, $constraints);

		return $schema;
	}

	/**
	 * @depends testCanAddConstraint
	 */
	public function testCanRemoveConstraint($schema) {
		//remove all constraints
		$schema->removeConstraint(get_class($this->getConstraint()));
		$constraints = $schema->getConstraints();
		$this->assertCount(0, $constraints);
		$this->assertEmpty($constraints);
	}

	protected function getSchema(): SchemaInterface {
		return new Schema();
	}

	protected function getConstraint() {
		return $this->createMock(Email::class);
	}
}