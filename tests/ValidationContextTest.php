<?php 
declare(strict_types=1);
namespace DarioRieke\Validation\Tests\Constraint;

use PHPUnit\Framework\TestCase;

use DarioRieke\Validation\ValidationContext;
use DarioRieke\Validation\ValidationContextInterface;
use DarioRieke\Validation\Violation\ViolationListInterface;
use DarioRieke\Validation\Violation\ViolationInterface;
use DarioRieke\Validation\ValidatorInterface;


class ValidationContextTest extends TestCase { 

	public function setUp(): void {
		$this->context = $this->getValidationContext();
	}

	public function testImplementsValidationContextInterface() {
		$this->assertInstanceOf(ValidationContextInterface::class, $this->context);
	}

	public function testCanAddViolation() {
		$this->context->addViolation($this->getViolation());

		$this->assertTrue(true);
	}

	public function testCanReturnViolationList() {
		$this->assertInstanceOf(ViolationListInterface::class, $this->context->getViolations());
	}

	public function testCanSetAndGetPath() {
		$this->context->setPath('test');
		$this->assertSame($this->context->getPath(), 'test');
	}

	public function testCanSetAndGetCurrentValue() {
		$this->context->setCurrentValue('test');
		$this->assertSame($this->context->getCurrentValue(), 'test');
	}

	public function testCanSetAndGetCurrentValueExists() {
		$this->context->setCurrentValueExists(true);
		$this->assertSame($this->context->currentValueExists(), true);	
	}

	public function testCanSetAndGetValidator() {
		$this->context->setValidator($this->getValidator());
		$this->assertInstanceOf(ValidatorInterface::class, $this->context->getValidator());
	}

	//merge violations is not tested as it is just a thin wrapper

	protected function getValidationContext() {
		return new ValidationContext($this->getViolationList());
	}

	protected function getViolationList() {
		return $this->createMock(ViolationListInterface::class);
	}

	protected function getViolation() {
		return $this->createMock(ViolationInterface::class);
	}

	protected function getValidator() {
		return $this->createMock(ValidatorInterface::class);
	}
}