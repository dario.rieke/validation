<?php 
declare(strict_types=1);
namespace DarioRieke\Validation\Tests;

use PHPUnit\Framework\TestCase;
use DarioRieke\Validation\ValidatorInterface;
use DarioRieke\Validation\ValidatorFactoryInterface;
use DarioRieke\Validation\ValidatorFactory;



class ValidatorFactoryTest extends TestCase { 

	public function setUp(): void {
		$this->validatorFactory = new ValidatorFactory();
	}

	public function testImplementsValidatorFactoryInterface() {
		$this->assertInstanceOf(ValidatorFactoryInterface::class, $this->validatorFactory);
	}	 

	public function testCreatesValidator() {
		$this->assertInstanceOf(ValidatorInterface::class, $this->validatorFactory->getValidator());
	}
}