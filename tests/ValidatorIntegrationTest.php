<?php 
declare(strict_types=1);
namespace DarioRieke\Validation\Tests;

use PHPUnit\Framework\TestCase;
use DarioRieke\Validation\Validator;
use DarioRieke\Validation\ValidatorInterface;
use DarioRieke\Validation\ValidatorFactory;
use DarioRieke\Validation\ValidatorFactoryInterface;
use DarioRieke\Validation\Tests\Fixtures\Validatable;
use DarioRieke\Validation\Constraint\Valid;


class ValidatorIntegrationTest extends TestCase { 

	public function setUp(): void {
		$this->validator = $this->getValidator();
	}

	protected function getValidator(): ValidatorInterface {
		return $this->getValidatorFactory()->getValidator();
	}

	protected function getValidatorFactory(): ValidatorFactoryInterface {
		return new ValidatorFactory();
	}

	protected function getValidValidatableClass() {
		$validatable = new Validatable();
		$validatable->setPrivateSchema('test');
		$validatable->setProtectedSchema('test1');
		$validatable->publicSchema = 'test2';

		return $validatable;
	}

	protected function getInvalidValidatableClass() {
		$validatable = new Validatable();

		return $validatable;
	}

	public function testCanValidateValidObjectImplementingValidatableInterface() {
		$errors = $this->validator->validate($this->getValidValidatableClass());

		$this->assertCount(0, $errors);
	}

	public function testCanNotValidateInvalidObjectImplementingValidatableInterface() {
		$errors = $this->validator->validate($this->getInvalidValidatableClass());

		$this->assertCount(3, $errors);
		return $errors;
	}

	public function testCanRecursivelyValidateValidObjectImplementingValidatableInterface() {
		$validatable = $this->getValidValidatableClass();

		//tell the schema to validate children recursively
		$schema = $validatable->getValidationSchema();
		foreach ($schema->getChildren() as $schema) {
			$schema->addConstraint(new Valid);			
		}

		$validatable->publicSchema = $this->getValidValidatableClass();
		$validatable->setProtectedSchema($this->getValidValidatableClass());
		$validatable->setPrivateSchema($this->getValidValidatableClass());

		$errors = $this->validator->validate($validatable);
		
		$this->assertCount(0, $errors);
	}

	public function testCanNotRecursivelyValidateInvalidObjectImplementingValidatableInterface() {
		$validatable = $this->getValidValidatableClass();

		//tell the schema to validate children recursively
		$schema = $validatable->getValidationSchema();
		foreach ($schema->getChildren() as $schema) {
			$schema->addConstraint(new Valid);			
		}

		$validatable->publicSchema = $this->getInvalidValidatableClass(); // +3 validation errors
		$validatable->setProtectedSchema(null); // + 1 validation error
		$validatable->setPrivateSchema($this->getInvalidValidatableClass()); // + 3 validation errors

		$errors = $this->validator->validate($validatable);
		$this->assertCount(7, $errors);

		return $errors;
	}

	/**
	 * @depends testCanNotRecursivelyValidateInvalidObjectImplementingValidatableInterface
	 */
	public function testPathOfViolationsMatchExpectationRecursive($violationListInterface) {
		$paths = [
			'object(DarioRieke\Validation\Tests\Fixtures\Validatable).publicSchema.publicSchema',
			'object(DarioRieke\Validation\Tests\Fixtures\Validatable).publicSchema.protectedSchema',
			'object(DarioRieke\Validation\Tests\Fixtures\Validatable).publicSchema.privateSchema',
			'object(DarioRieke\Validation\Tests\Fixtures\Validatable).protectedSchema',
			'object(DarioRieke\Validation\Tests\Fixtures\Validatable).privateSchema.publicSchema',
			'object(DarioRieke\Validation\Tests\Fixtures\Validatable).privateSchema.protectedSchema',
			'object(DarioRieke\Validation\Tests\Fixtures\Validatable).privateSchema.privateSchema'
		];
		foreach ($violationListInterface as $violation) {
			$this->assertContains($violation->getPath(), $paths);
		}
	}

	/**
	 * @depends testCanNotValidateInvalidObjectImplementingValidatableInterface
	 */
	public function testPathOfViolationsMatchExpectation($violationListInterface) {

		$paths = [
			'object(DarioRieke\Validation\Tests\Fixtures\Validatable).publicSchema',
			'object(DarioRieke\Validation\Tests\Fixtures\Validatable).protectedSchema',
			'object(DarioRieke\Validation\Tests\Fixtures\Validatable).privateSchema'
		];

		foreach ($violationListInterface as $violation) {
			$this->assertContains($violation->getPath(), $paths);
		}

	}
}